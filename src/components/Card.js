import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, Animated } from 'react-native';



function generatePath(name){
    switch (name) {
        case 'abra.png': return require('../../assets/frontcard/abra.png')
        case 'bullbasaur.png': return require('../../assets/frontcard/bullbasaur.png')
        case 'caterpie.png': return require('../../assets/frontcard/caterpie.png')
        case 'charmander.png': return require('../../assets/frontcard/charmander.png')
        case 'eevee.png': return require('../../assets/frontcard/eevee.png')
        case 'jigglypuff.png': return require('../../assets/frontcard/jigglypuff.png')
        case 'meowth.png': return require('../../assets/frontcard/meowth.png')
        case 'mew.png': return require('../../assets/frontcard/mew.png')
        case 'pikachu.png': return require('../../assets/frontcard/pikachu.png')
        case 'pokeball.png': return require('../../assets/frontcard/pokeball.png')
        case 'psyduck.png': return require('../../assets/frontcard/psyduck.png')
        case 'razz-berry.png': return require('../../assets/frontcard/razz-berry.png')
        case 'snorlax.png': return require('../../assets/frontcard/snorlax.png')
        case 'squirtle.png': return require('../../assets/frontcard/squirtle.png')
        case 'superball.png': return require('../../assets/frontcard/superball.png')
        case 'zubat.png': return require('../../assets/frontcard/zubat.png')
    }
}


export default class Card extends Component {
    constructor(props) {
        super(props);
    }
    componentWillMount() {
        this.animatedValue = new Animated.Value(0);
        this.value = 0;
        this.animatedValue.addListener(({value}) => {
            this.value = value;
        })
        this.frontInterpolate = this.animatedValue.interpolate({
            inputRange: [0, 180],
            outputRange: ['0deg', '180deg'],
        });
        this.backInterpolate = this.animatedValue.interpolate({
            inputRange: [0,180],
            outputRange: ['180deg', '360deg'],
        })
    };
    flipCard() {
        if (this.value >= 90) {
            Animated.spring(this.animatedValue, {
                toValue: 0,
                friction: 8,
                tension: 10
            }).start();
        } else {
            Animated.spring(this.animatedValue, {
                toValue: 180,
                friction: 8,
                tension: 10
            }).start();
        }
    }
    render() {
        const frontAnimatedStyle = {
            transform: [
                { rotateY: this.frontInterpolate }
            ]
        };
        const backAnimatedStyle = {
            transform: [
                { rotateY: this.backInterpolate }
            ]
        };
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => {

                    if(this.props.currentSelection.length < 2) {
                        this.flipCard()
                    }
                    this.props.updateSelection({id: this.props.id, name: this.props.name})}}>
                    <View>
                        <Animated.View style={[styles.flipCard, frontAnimatedStyle]}>
                            <Image style={{height: '80%', width: '80%'}} source={require('../../assets/hearthstone.jpg')}/>
                        </Animated.View>
                        <Animated.View style={[styles.flipCard, styles.flipCardBack, backAnimatedStyle]}>
                            <Image style={{height: '50%', width: '50%'}} source={generatePath(this.props.name)}/>
                        </Animated.View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    cardContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    flipCard: {
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: '#d48003',
        width: 75,
        height: 125,
        margin: 5,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.8,
        shadowRadius: 2,
        alignItems: 'center',
        justifyContent: 'center',
        backfaceVisibility: 'hidden'
    },
    flipCardBack: {
        backgroundColor: 'white',
        position: 'absolute',
        backfaceVisibility: 'hidden'
    }
});
