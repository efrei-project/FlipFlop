import React from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';
import { Stopwatch } from 'react-native-stopwatch-timer';
import StopWatch from "react-native-stopwatch-timer/lib/stopwatch";


const getFormattedTime = (time) => {
      this.currentTime = time;
}

let timer = false;

let Score = (props) => {
    return (
        <View >
            <View style={style.container} >
                <Stopwatch
                    laps
                    start={timer}
                    getTime={getFormattedTime}
                />
                <Text style={style.textBold}>Score:</Text>
                <Text>{props.score} points</Text>
                <Text style={style.textBold}>Player:</Text>
                <Text >{props.userName}</Text>
            </View>
            <View style={style.container} >
                <Button title={"Easy"} onPress={() => {props.changeBoard(4); timer = true;}}/>
                <Button title={"Medium"} onPress={() => {props.changeBoard(8); timer = true;}}/>
                <Button title={"Hard"} onPress={() => {props.changeBoard(16); timer = true;}}/>
            </View>

        </View>
    )
};


const style = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        borderWidth: 1,
        width: '100%',
        height: 30,
        borderColor: 'black',
        backgroundColor: '#e6f9ff',
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.8,
        shadowRadius: 2,
    },
    textBold: {
        fontWeight: 'bold',
    }
})


export default Score;
