# FlipFlop
School's project to learn React-Native


Success:
 - flip card
 - set name of the player
 - set score when two cards are found (actual bug in isMatching algo)
 - set difficulty and reload gameboard
 
 
 Fail:
 - flip back card when doesn't match
 - bug in isMatching() algo
 - stats of games (bonus features)
