import React from 'react';
import { StyleSheet, Text, View, AlertIOS } from 'react-native';
import { LinearGradient } from 'expo';
import Home from './src/components/Home';
import Header from './src/components/Header';
import Score from './src/components/Score';

function generateList(nb=16){
  let list = [];
  const dict = [
      {id: 1,  name: 'abra.png',        ite: 0},
      {id: 2,  name: 'bullbasaur.png',  ite: 0},
      {id: 3,  name: 'caterpie.png',    ite: 0},
      {id: 4,  name: 'charmander.png',  ite: 0},
      {id: 5,  name: 'eevee.png',       ite: 0},
      {id: 6,  name: 'jigglypuff.png',  ite: 0},
      {id: 7,  name: 'meowth.png',      ite: 0},
      {id: 8,  name: 'mew.png',         ite: 0},
      {id: 9,  name: 'pikachu.png',     ite: 0},
      {id: 10, name: 'pokeball.png',    ite: 0},
      {id: 11, name: 'psyduck.png',     ite: 0},
      {id: 12, name: 'razz-berry.png',  ite: 0},
      {id: 13, name: 'snorlax.png',     ite: 0},
      {id: 14, name: 'squirtle.png',    ite: 0},
      {id: 15, name: 'superball.png',   ite: 0},
      {id: 16, name: 'zubat.png',       ite: 0},
  ];

  while(true){
      let index = Math.floor(Math.random() * Math.floor(nb / 2));
      let card = dict[index];

      if(card.ite == 0){
          dict[index].ite = dict[index].ite + 1;
          list.push({id: card.id, name: card.name});
      } else if (card.ite == 1) {
          dict[index].ite = dict[index].ite + 1;
          list.push({id: card.id, name: card.name});
      }
      if (list.length == nb){
          break;
      }
  }

  return list
}


export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      userName: "",
      score: 0,
      currentSelection: [],
      difficulty: 0,
      list: [],
    }
    this.updateSelection = this.updateSelection.bind(this);
    this.checkIfMatch = this.checkIfMatch.bind(this)
  }
  componentDidMount() {
    AlertIOS
        .prompt('Please enter your name', null,
            (text) => this.setUserName(text));
  }
  setDifficulty(nb){
      this.setState({difficulty: nb, list: generateList(nb)});

  }
  setUserName(userName) {
    if(userName === "") {
      this.setState({userName: "user"})
    } else {
      userName = userName.slice(0,8)
      this.setState({userName})
    }
  }

  changeBoard(nb){
      console.log(this)
      this.setState({list: generateList(nb)})
  }

  updateSelection(selection) {
    let copyOfCurrentSelection = this.state.currentSelection.slice();
    copyOfCurrentSelection.push(selection);
    this.setState({currentSelection: copyOfCurrentSelection});
  }
  checkIfMatch() {
    let copyOfList = this.state.list.slice();
    console.log(`this.state.currentSelection.length: ${this.state.currentSelection.length}`);
    if(this.state.currentSelection.length >= 2) {
      if(this.state.currentSelection[0].name === this.state.currentSelection[1].name &&
          this.state.currentSelection[0].id !== this.state.currentSelection[1].id) {
        for(var i = 0; i < copyOfList.length; i++) {
          if(copyOfList[i].id === this.state.currentSelection[0].id
              || copyOfList[i].id === this.state.currentSelection[1].id) {
            copyOfList.splice(i, 1)
            i = 0;
          }
        }
        this.setState({score: this.state.score + 1})
        this.setState({list: copyOfList})
        this.setState({currentSelection: []})
      } else {
        this.setState({currentSelection: []})
      }
    }
  }
  render() {
    this.checkIfMatch();
    console.log('this is state', this.state)
    return (
        <View style={styles.container}>
          <LinearGradient
              colors={['#ff5524', 'transparent']}
              style={styles.gradient}
          />
          <Header/>
          <Score userName={this.state.userName} score={this.state.score} changeBoard={this.changeBoard.bind(this)}/>
          <Home currentSelection={this.state.currentSelection} updateSelection={this.updateSelection} cards={this.state.list}/>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbaf0d'
  },
  gradient: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    height: 600,
    minHeight: 600,
  }
});















